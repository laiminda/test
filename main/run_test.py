#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os.path
import sys

p = os.path.dirname(os.path.dirname((os.path.abspath(__file__))))
if p not in sys.path:
    sys.path.append(p)
import unittest
from datetime import datetime

from util.HTMLTestRunner import HTMLTestRunner
from util.auxiliary_tool import mkdir, mkfile, print_time
from util.runmethod import RunMethod
from data.get_data import GetData
from util.common_assert import CommonUtil
import json
from data.dependent_data import DependentData
from util.send_mail import SendEmail
from util.print_log import initLogging


class RunTest(unittest.TestCase):

    def __init__(self):
        super().__init__()
        self.run_method = RunMethod()
        self.data = GetData()
        self.com_util = CommonUtil()
        self.send_mail = SendEmail()

    # 程序执行
    def test_run(self):
        res = None
        pass_count = []
        fail_count = []
        error_count = []
        no_run_count = []
        summary = {'success': {}, 'fail': {}, 'no_run': {}, 'error':{}}  # (用例ID,用例名,字段,期望值,实际值,状态)

        # 每次执行用例之前创建log日志文件
        #log_file = '../log/log.txt'
        #with open(log_file, 'w') as f:
        #    f.seek(0, 0)  # 加上f.seek(0)，把文件定位到position 0;没有这句的话，文件是定位到数据最后，truncate也是从最后这里删除
        #    f.truncate()
        mkdir()
        log_file = mkfile()
        while self.data.refresh_file(log_file):
            file_type = self.data.get_file_type()  # 0=xls文件   1=xlsx文件
            while self.data.turn_sheet(log_file):
                #self.data.refresh_config()
                rows_count = self.data.get_case_lines()
                for i in range(file_type + 1, rows_count + file_type):
                    try:
                        is_run = self.data.get_is_run(i)
                        if is_run:
                            url = self.data.get_request_url(i)
                            if url is None:
                                continue
                            method = self.data.get_request_method(i)
                            # 获取请求参数
                            data = self.data.get_body_data_value(i)
                            # 获取excel文件中header关键字
                            cookies_key = self.data.get_request_cookie_key(i)
                            # 获取json文件中header_key对应的头文件数据
                            header = self.data.get_header_value(i)
                            # expect = self.data.get_expect_data(i)
                            # depend_case = self.data.is_depend(i)
                            depend_list = self.data.get_depend_info(i)
                            # if depend_case !=None:
                            for value in depend_list:     # (case_id, get_key, send_key, position)
                                depend_data = DependentData(value[0], self.data)
                                # 获取依赖字段的响应数据
                                depend_response_data = depend_data.get_value_for_key(value[1])
                                # 获取请求依赖的key
                                # depend_key = self.data.get_depend_field(i)
                                # position=self.data.get_position(i)
                                position = value[3].lower()
                                if position == 'url':
                                    url = '{url}&{key}={value}'.format(url=url, key=value[2], value=depend_response_data)
                                elif position == 'body':
                                    data = self.data.union_depend_data(data, value[2], depend_response_data)
                                elif position == 'header':
                                    header = self.data.union_depend_data(header, value[2], depend_response_data)
                                # 将依赖case的响应返回中某个字段的value赋值给该接口请求中某个参数
                                # data[depend_key] = depend_response_data
                            if cookies_key:
                                cookies = self.data.get_cookies_data_value(i)
                                res = self.run_method.run_main(method, url, header=header, cookie=cookies, params=data)
                            else:
                                res = self.run_method.run_main(method, url, data, header, params=data)

                            '''
                            get请求参数是params:request.get(url='',params={}),post请求数据是data:request.post(url='',data={})
                            excel文件中没有区分直接用请求数据表示,则data = self.data.get_data_value(i)拿到的数据，post请求就是data=data,get请就是params=data
                            '''
                            dur = print_time(str(self.run_method.elapsed))
                            results = self.com_util.statistics_result(self.data, i, res)
                            if results['result']:  # self.com_util.statistics_result(self.data, i, res):
                                self.data.write_result(i, 'pass')
                                pass_count.append(i)
                                summary['success'][self.data.get_current_document()+'&'+self.data.name+'&'+self.data.get_test_id(i)] = [self.data.get_test_id(i), self.data.get_test_name(i), dur, '', 'pass']
                            else:
                                # 返回的res是dict类型，要将res数据写入excel中，需将dict类型转换成str类型
                                self.data.write_result(i, json.dumps(res))
                                summary['fail'][self.data.get_current_document()+'&'+self.data.name+'&'+self.data.get_test_id(i)] = [self.data.get_test_id(i), self.data.get_test_name(i), dur, results['wrong'].items(), 'fail']
                                with open(log_file, 'a', encoding='utf-8') as f:
                                    f.write("\n用例<%s>实际结果与预期结果不一致:\n" % self.data.get_test_id(i))
                                    for key, value in results['wrong'].items():
                                        f.write("Key:%s\nExpected:%s\nActual:%s\n-------\n" % (key, value[0], value[1]))
                                    # f.write("Expected:%s\n  Actual:%s\n" % (expect, res))
                                fail_count.append(i)
                        else:
                            no_run_count.append(i)
                            summary['no_run'][self.data.get_current_document() + '&' + self.data.name + '&' + self.data.get_test_id(i)] = [self.data.get_test_id(i), self.data.get_test_name(i), -1, '', 'no_run']

                    except Exception as e:
                        summary['error'][self.data.get_current_document()+'&'+self.data.name+'&'+self.data.get_test_id(i)] = [self.data.get_test_id(i), self.data.get_test_name(i), -1, str(e), 'error']
                        # 将异常写入excel的测试结果中
                        self.data.write_result(i, str(e))
                        # 将报错写入指定路径的日志文件里
                        with open(log_file, 'a', encoding='utf-8') as f:
                            f.write("\n用例<%s>报错:\n" % self.data.get_test_id(i))
                        initLogging(log_file, e)
                        error_count.append(i)
                        #raise e
                    self.run_method.elapsed = -1
        self.send_mail.send_main(self.data.filelist, pass_count, fail_count, error_count, no_run_count)
        return {'success': pass_count, 'fail': fail_count, 'error': error_count, 'no_run': no_run_count, 'summary': summary}


if __name__ == '__main__':
    s_time = datetime.now()
    run = RunTest()
    mkdir(path='../report')
    html_file = mkfile(path='../report', extension='.html')
    st = open(html_file, 'wb')
    #st = open('../report/report.html', 'wb')
    HTMLTestRunner(stream=st, title=u'接口自动化测试报告', description=u'测试者：laiminda').run(run)
    e_time = datetime.now()