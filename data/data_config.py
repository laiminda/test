#!/usr/bin/python
# -*- coding: UTF-8 -*-
from util.operation_excel import OperationExcel


class data_config():
    def __init__(self):
        self.index = 0

    # 获取caseid下标
    def get_id(self):
        return self.dictLine0['Id']#self.Id

    # 设置caseid下标
    def set_id(self,index):
        self.Id = index

    # 获取name下标
    def get_name(self):
        return self.dictLine0['名称']#self.name

    # 设置name下标
    def set_name(self,index):
        self.name = index

    # 获取url下标
    def get_url(self):
        return self.dictLine0['url']#self.url

    # 设置url下标
    def set_url(self,index):
        self.url = index

    # 获取run下标
    def get_run(self):
        return self.dictLine0['是否执行']#self.run

    # 设置run下标
    def set_run(self,index):
        self.run = index

    # 获取request_way下标
    def get_request_way(self):
        return self.dictLine0['请求类型']#self.request_way

    # 设置request_way下标
    def set_request_way(self,index):
        self.request_way = index

    # 获取request_cookie_key下标
    def get_cookie_key(self):
        return self.dictLine0['是否cookies发送']#self.request_cookie_key

    # 设置request_cookie_key下标
    def set_cookie_key(self,index):
        self.request_cookie_key = index

    # 获取request_cookie_data下标
    def get_cookie_data(self):
        return self.dictLine0['cookies数据']#self.request_cookie_data

    # 设置request_cookie_data下标
    def set_cookie_data(self,index):
        self.request_cookie_data = index

    # 获取request_header下标
    def get_header(self):
        return self.dictLine0['请求头header']#self.request_header

    # 设置request_header下标
    def set_header(self,index):
        self.request_header = index

    # 获取case_depend下标
    def get_case_depend(self):
        return self.dictLine0['case依赖']#self.case_depend

    # 设置case_depend下标
    def set_case_depend(self,index):
        self.case_depend = index

    # 获取data_depend下标
    def get_data_depend(self):
        return self.dictLine0['依赖的返回数据']#self.data_depend

    # 设置data_depend下标
    def set_data_depend(self,index):
        self.data_depend = index

    # 获取field_depend下标
    def get_field_depend(self):
        return self.dictLine0['数据依赖字段']#self.field_depend

    # 设置field_depend下标
    def set_field_depend(self,index):
        self.field_depend = index

    # 获取请求数据data下标
    def get_data(self):
        return self.dictLine0['请求数据']#self.request_data

    # 设置请求数据data下标
    def set_data(self,index):
        self.request_data = index

    # 获取except下标
    def get_expect(self):
        return self.dictLine0['预期结果']#self.expect

    # 设置except下标
    def set_expect(self,index):
        self.expect = index

    # 获取result下标
    def get_result(self):
        return self.dictLine0['实际结果']#self.result

    # 设置result下标
    def set_result(self,index):
        self.result = index

    # 获取position下标
    def get_position(self):
        return self.dictLine0['依赖字填写位置']#self.position

    # 设置position下标
    def set_position(self, index):
        self.position = index

    def sort_dict(self):
        items = self.dictLine0.items()
        backitems = [[v[1], v[0]] for v in items]
        backitems.sort()
        items = [(v[1], v[0]) for v in backitems]
        return items

    def get_next(self, index):
        flag = 0
        for pair in self.diclis:
            if flag and pair[0] is not None and pair[0] in self.titlelist:
                return pair[1]
            if pair[1] == index:
                flag = 1
                continue
        return -2

    # 刷新所有下标
    def refresh_config(self, oper_excel):
        self.line0 = oper_excel.get_row_values(self.index)
        self.dictLine0 = {'Id': -1, '名称': -1, 'url': -1, '是否执行': -1, '请求类型': -1,
                          '是否cookies发送': -1, 'cookies数据': -1, '请求头header': -1, 'case依赖': -1, '依赖的返回数据': -1,
                          '数据依赖字段': -1, '依赖字填写位置': -1, '请求数据': -1, '预期结果': -1, '实际结果': -1}
        self.titlelist = self.dictLine0.keys()
        for title in self.line0:
            index = self.line0.index(title)+self.index
            self.dictLine0[title] = index
            if title == 'Id':
                self.set_id(index)
            elif title == '名称':
                self.set_name(index)
            elif title == 'url':
                self.set_url(index)
            elif title == '是否执行':
                self.set_run(index)
            elif title == '请求类型':
                self.set_request_way(index)
            elif title == '是否cookies发送':
                self.set_cookie_key(index)
            elif title == 'cookies数据':
                self.set_cookie_data(index)
            elif title == '请求头header':
                self.set_header(index)
            elif title == 'case依赖':
                self.set_case_depend(index)
            elif title == '依赖的返回数据':
                self.set_data_depend(index)
            elif title == '数据依赖字段':
                self.set_field_depend(index)
            elif title == '依赖字填写位置':
                self.set_position(index)
            elif title == '请求数据':
                self.set_data(index)
            elif title == '预期结果':
                self.set_expect(index)
            elif title == '实际结果':
                self.set_result(index)
        self.diclis = self.sort_dict()


