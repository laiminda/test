#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json
import os.path
import re

from util.operation_excel import OperationExcel, OperationXlsx
from data import data_config
from  util.operation_json import OperationJson

class GetData:
    def __init__(self):
        self.excel_config = data_config.data_config()
        self.filelist=self.search()
        self.filenum=len(self.filelist)
        self.index=0

    # 去获取excel的sheet数
    def get_sheet_lines(self):
        return self.oper_excel.get_sheet_num()

    def turn_sheet(self, log='../log/log.txt'):
        self.name = self.oper_excel.turn_page()
        flag = (self.name is not None)
        if flag:
            self.excel_config.refresh_config(self.oper_excel)
            with open(log, 'a', encoding='utf-8') as f:
                f.write('\n<---------------------------Excel SheetName:%s--------------------------->\n\n' % self.name)
        return flag

    #去获取excel行数，就是case个数
    def get_case_lines(self):
        return self.oper_excel.get_lines()

    #获取是否执行
    def get_is_run(self, row):
        flag = None
        col = self.excel_config.get_run()
        if col == -1:
            return False
        run_model = self.oper_excel.get_cell_value(row,col)
        if run_model == 'yes':
            flag = True
        else:
            flag = False
        return flag

    #获取请求方式
    def get_request_method(self,row):
        col = self.excel_config.get_request_way()
        if col == -1:
            return None
        request_method = self.oper_excel.get_cell_value(row, col)
        return request_method

    #获取url
    def get_request_url(self,row):
        col = self.excel_config.get_url()
        if col == -1:
            return None
        url = self.oper_excel.get_cell_value(row,col)
        return url

    # 拼装请求数据
    def parse_str_dict(self, list, value, dicto=None):
        dict={}
        key=list[0].strip()
        if len(list) == 1:
            dict[key] = value
            return dict
        if dicto.get(key) is None:
            dict[key]=self.parse_str_dict(list[1:],value,{})
        else:
            dict[key] = dicto[key]
            dict[key].update(self.parse_str_dict(list[1:], value, dict[key]))
        return dict

    # 把字典类型转换成列表
    def dict_to_list(self, dictin):
        for key in list(dictin.keys()):
            dic = dictin[key]
            if isinstance(dic, dict):
                dic=self.dict_to_list(dic)
            if '[' in key:
                list1 = key.split('[')
                list1[0]=list1[0].strip()
                if list1[0] not in dictin.keys():
                    dictin[list1[0]] = []
                dictin[list1[0]].append(dic)
                del dictin[key]
        #print(dictin)
        return dictin

    # 根据文档参数拼接json结构
    def splicing_json(self, row, col, len):
        dict1 = {}
        i = 0
        flag = True
        if len == 0:
            flag = False
        while True:
            if i == len and flag:
                break
            param = self.oper_excel.get_cell_value(row, col + i)
            if param == '' or param is None:
                break
            index = param.find('=')
            keys = param[0:index]
            keys = keys.strip()
            value = param[index + 1:]
            value = value.strip()
            if value == '\"\"':
                value=''
            if '.' in keys:
                kd = keys.split('.')
                dict1.update(self.parse_str_dict(kd, value, dicto=dict1))
            else:
                dict1[keys] = value
            i += 1
        return dict1

    def union_depend_data(self, data, input, value):
        key_list = input.split('.')
        data = data.update(self.parse_str_dict(key_list, value, dicto=data))
        data = self.dict_to_list(data)
        return data

    # 获取请求头header
    def get_request_header(self, row):
        col = self.excel_config.get_header()
        if col == -1:
            return '{}'
        col_n = self.excel_config.get_next(col)
        len = 0
        if col_n > col:
            len = col_n - col
        #len = self.excel_config.get_case_depend() - col
        #data = self.oper_excel.get_cell_value(row,col)
        dict1 = self.splicing_json(row, col, len)
        if dict1.__len__() == 0:
            return None
        dict1 = self.dict_to_list(dict1)
        str = json.dumps(dict1)
        #print(str)
        return str

    # 获取请求头header
    def get_request_cookie_key(self, row):
        col = self.excel_config.get_cookie_key()
        if col == -1:
            return False
        run_model = self.oper_excel.get_cell_value(row, col)
        if run_model.lower() == 'yes':
            flag = True
        else:
            flag = False
        return flag
    # 通过获取头数据
    def get_header_value(self, row):
        oper_json = OperationJson('../dataconfig/request_header.json')
        request_header = oper_json.union_data(self.get_request_header(row))
        return request_header

    #获取请求数据
    def get_request_data(self,row):
        col = self.excel_config.get_data()
        if col == -1:
            return '{}'
        col_n = self.excel_config.get_next(col)
        len = 0
        if col_n > col:
            len = col_n - col
        #len = self.excel_config.get_next(col) - col
        #len = self.excel_config.get_expect() - col
        dict1 = self.splicing_json(row, col, len)
        if dict1.__len__() == 0:
            return None
        dict1 = self.dict_to_list(dict1)
        str=json.dumps(dict1)
        #print(str)
        return str

    def get_depend_info(self,row):
        col = self.excel_config.get_case_depend()
        depend_list = []
        if col == -1:
            return depend_list
        col_n = self.excel_config.get_next(col)
        len = 0
        if col_n > col:
            len = col_n - col
        #len = self.excel_config.get_next(col) - col
        #len = self.excel_config.get_data() - col
        for i in range(0, len, 4):
            case_id = self.oper_excel.get_cell_value(row, col+i)
            if case_id == '' or case_id is None:
                continue
            get_key = self.oper_excel.get_cell_value(row, col + i + 1)
            send_key = self.oper_excel.get_cell_value(row, col + i + 2)
            position = self.oper_excel.get_cell_value(row, col + i + 3)
            depend_list.append((case_id, get_key, send_key, position))
        return depend_list

    # 获取cookies数据
    def get_cookies_data(self, row):
        col = self.excel_config.get_cookie_data()
        if col == -1:
            return '{}'
        col_n = self.excel_config.get_next(col)
        len = 0
        if col_n > col:
            len = col_n - col
        #len = self.excel_config.get_header() - col
        #data = self.oper_excel.get_cell_value(row, col)
        dict1 = self.splicing_json(row, col, len)
        if dict1.__len__() == 0:
            return None
        dict1 = self.dict_to_list(dict1)
        str = json.dumps(dict1)
        # print(str)
        return str

    # 通过获取消息体数据
    def get_body_data_value(self,row):
        oper_json = OperationJson('../dataconfig/request_data.json')
        request_data = oper_json.union_data(self.get_request_data(row))
        return request_data

    # 通过获取cookies数据
    def get_cookies_data_value(self, row):
        oper_json = OperationJson('../dataconfig/cookie.json')
        request_data = oper_json.union_data(self.get_cookies_data(row))
        return request_data

    #通过获取请求关键字拿到data数据
    def get_data_value(self,row):
        oper_json = OperationJson('../dataconfig/request_data.json')
        request_data = oper_json.get_data(self.get_request_data(row))
        return request_data


    #获取预期结果
    def get_expect_data(self,row):
        col = self.excel_config.get_expect()
        expect = self.oper_excel.get_cell_value(row,col)
        return expect

    #写入数据
    def write_result(self,row,value):
        col = self.excel_config.get_result()
        self.oper_excel.write_value(row, col, value)

    #获取依赖数据的key
    def get_depend_key(self,row):
        col = self.excel_config.get_data_depend()
        if col == -1:
            return None
        depend_key = self.oper_excel.get_cell_value(row, col)
        if depend_key == '':
            return None
        else:
            return depend_key

    #判断是否有case依赖
    def is_depend(self,row):
        col = self.excel_config.get_case_depend()
        if col == -1:
            return None
        depend_case_id = self.oper_excel.get_cell_value(row, col)
        if depend_case_id == '':
            return None
        else:
            return depend_case_id

    #获取请求依赖字段
    def get_depend_field(self,row):
        col = self.excel_config.get_field_depend()
        if col == -1:
            return None
        data = self.oper_excel.get_cell_value(row, col)
        if data == '':
            return None
        else:
            return data

    # 获取请求依赖字段
    def get_position(self, row):
        col = self.excel_config.get_position()
        if col == -1:
            return None
        data = self.oper_excel.get_cell_value(row, col)
        if data == '':
            return None
        else:
            return data

    # 获取请求依赖字段
    def search(self, path='../case/'):
        filelist=[]
        regex1=re.compile(r'\.xlsx$')
        regex2 = re.compile(r'\.xls$')
        for filename in os.listdir(path):
            fp=os.path.join(path,filename)
            if os.path.isfile(fp) and (regex2.search(filename) or regex1.search(filename)): #('.xls' in filename or '.xlsx' in filename):
                filelist.append(fp)
            elif os.path.isdir(fp):
                filelist.extend(self.search(fp))
        return filelist

    # 获取下一个文档
    def refresh_file(self, log='../log/log.txt'):
        if self.filenum==self.index:
            return False
        if '.xls' == self.filelist[self.index][-4:]:
            self.oper_excel=OperationExcel(self.filelist[self.index])
            self.excel_config.index = 0
        #elif '.xlsx' == self.filelist[self.index][-5:]:
        else:
            self.oper_excel = OperationXlsx(self.filelist[self.index])
            self.excel_config.index = 1
        #else:
        #    print(self.filelist[self.index])
        #    self.index += 1
        #    return self.refresh_file()
        with open(log, 'a', encoding='utf-8') as f:
            f.write('\n\n\n<---------------------------Excel Document:%s--------------------------->\n' % (self.filelist[self.index]))
        self.index += 1
        return True

    # 获取文件类型 0.xls文件  1.xlsx文件
    def get_file_type(self):
        return self.excel_config.index

    # 设置文件格式配置信息
    def refresh_config(self):
        self.excel_config.refresh_config(self.oper_excel)

    # 获取测试用例id
    def get_test_id(self, row):
        id = self.oper_excel.get_cell_value(row, self.excel_config.get_id())
        if isinstance(id, type(None)):
            id = ''
        return id

    def get_test_name(self, row):
        name = self.oper_excel.get_cell_value(row, self.excel_config.get_name())
        if isinstance(name, type(None)):
            name = ''
        return name

    def get_current_document(self):
        return self.filelist[self.index-1]

    def get_file_name(self):
        return self.name