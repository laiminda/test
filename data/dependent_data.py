#!/usr/bin/python
# -*- coding: UTF-8 -*-
from time import sleep

from util.operation_excel import OperationExcel, OperationXlsx
from util.runmethod import RunMethod
from data.get_data import GetData
from jsonpath_rw import parse

class DependentData:

    def __init__(self, case_id, data):
        self.case_id = case_id
        self.oper_excel = data.oper_excel
        self.method = RunMethod()
        self.data = data

    # 通过case_id去获取依赖case_id的整行数据
    def get_case_line_data(self):
        rows_data = self.oper_excel.get_rows_data(self.case_id)
        return rows_data

    #执行依赖测试，获取结果
    def run_dependent(self):
        row_num = self.oper_excel.get_row_num(self.case_id)
        request_data = self.data.get_body_data_value(row_num)
        header = self.data.get_header_value(row_num)
        method = self.data.get_request_method(row_num)
        url = self.data.get_request_url(row_num)
        sleep(3)
        res = self.method.run_main(method, url, request_data, header, params=request_data)
        sleep(3)
        return res

    #获取依赖字段的响应数据：通过执行依赖测试case来获取响应数据，响应中某个字段数据作为依赖key的value
    def get_value_for_key(self, key):
        #获取依赖的返回数据key
        #depend_data = self.data.get_depend_key(row)
        #执行依赖case返回结果
        response_data = self.run_dependent()
        return [match.value for match in parse(key).find(response_data)][0]
