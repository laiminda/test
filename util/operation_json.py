#!/usr/bin/python
# -*- coding: UTF-8 -*-
import ast
import json

class OperationJson:

    def __init__(self,file_path):
        self.file_path = file_path
        self.data = self.read_data()

    #读取json文件
    def read_data(self):
        with open(self.file_path) as fp:
            data = json.load(fp)
            return data

    #根据关键字获取数据
    def get_data(self,key):
        # return self.data[key]
        return self.data.get(key)

    # 结合全局head数据和自定义head数据
    def union_data(self, xldata=None):
        if isinstance(xldata, dict):
            self.data.update(xldata)
            return self.data
        if xldata == None:
            return self.data
        else:
            try:
                dict1 = ast.literal_eval(xldata)
            except Exception:
                dict1 = json.loads(xldata)
            self.data.update(dict1)
            return self.data

    # 将cookies数据写入json文件
    def write_data(self, data):
        with open('../dataconfig/cookie.json', 'w') as fp:
            fp.write(json.dumps(data))



