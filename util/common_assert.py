#!/usr/bin/python
# -*- coding: UTF-8 -*-
import ast
import re
from jsonpath_rw import parse

class CommonUtil:
    #判断一个字符串是否在另外一个字符串中
    def is_contain(self,str_one,str_two):
        '''
        str_one: 查找的字符串
        str_two:  被查找的字符串
        '''
        flag = None
        if str_one in str_two:
            flag = True
        else:
            flag = False
        return flag

    # 判断一个字符串是否全是数字组成
    def is_num(self, str):
        try:
            complex(str)
        except ValueError:
            return False
        return True

    # 判断实际字段值是否符合预期
    def is_expected(self, expect, result):
        kv=expect.split(':')
        relist=[]  #[对比字段，期望值，实际值]
        if re.search('\"',kv[0],flags=0):
            kv[0] = ast.literal_eval(kv[0])
        if re.search('\"',kv[1],flags=0):
            kv[1] = ast.literal_eval(kv[1])
        if self.is_num(kv[1]):
            kv[1]=int(kv[1])
        kv[0].encode('utf-8')
        relist.append(kv[0])
        relist.append(kv[1])
        try:
            relist.append([match.value for match in parse(kv[0]).find(result)][0])
        except Exception:
            relist.append("value is not alive")
        return relist #kv[1]==[match.value for match in parse(kv[0]).find(result)][0]

    # 统计一个测试用例的所有预期与实际结果对照
    def statistics_result(self, getdata, row, result):
        col = getdata.excel_config.get_expect()
        len = getdata.excel_config.get_next(col) - col
        results=True
        wrong={}
        reall={}
        for i in range(len):
            param = getdata.oper_excel.get_cell_value(row, col + i)
            res = self.is_expected(param, result)
            flag = (res[1] == res[2])
            if not flag:
                wrong[res[0]] = (res[1], res[2])
            results = results & flag
        reall['result'] = results
        reall['wrong'] = wrong
        return reall  #{'result':True/False,'wrong':{对比字段:(期望值，实际值)}}