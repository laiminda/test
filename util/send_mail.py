#!/usr/bin/python
# -*- coding: UTF-8 -*-
import smtplib
import time
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class SendEmail:

    def send_mail(self, files, receiver_list, sub, content):
        mail_host = "mail.dianhun.cn"  # 设置服务器
        mail_user = "laiminda@dianhun.cn"  # 用户名
        mail_pass = "789.dianhun"  # 口令

        sender = 'laiminda@dianhun.cn'
        # 构造附件1，传送当前目录下的 test.txt 文件
        filename = '../log/log.txt'
        f=open(filename, 'rb')
        mail_body1=f.read()
        f.close()
        att1 = MIMEText(mail_body1, 'txt', 'utf-8')
        att1["Content-Type"] = 'application/octet-stream'
        att1["Content-Disposition"] = 'attachment; filename="log.txt"'



        msg=MIMEMultipart()
        msg.attach(MIMEText(content, 'plain', 'utf-8'))
        msg['From'] = Header("shape" + "<" + mail_user + ">", 'utf-8')
        msg['To'] = Header("测试", 'utf-8')
        msg['Subject'] = Header(sub, 'utf-8')
        msg['date']=time.strftime("%a, %d %b %Y %H:%M:%S %z")
        msg.attach(att1)
        for file in files:
            filename = file
            f = open(filename, 'rb')
            mail_body2 = f.read()
            f.close()
            if '.xlsx' == file[-5:]:
                att2 = MIMEText(mail_body2, 'xlsx', 'utf-8')
            else:
                att2 = MIMEText(mail_body2, 'xlsx', 'utf-8')
            att2["Content-Type"] = 'application/octet-stream'
            att2["Content-Disposition"] = 'attachment; filename="interface.xls"'
            msg.attach(att2)

        try:
            smtpObj = smtplib.SMTP()
            smtpObj.connect(mail_host, 25)  # 25 为 SMTP 端口号
            smtpObj.login(mail_user, mail_pass)
            smtpObj.sendmail(sender, receiver_list, msg.as_string())
            print("邮件发送成功")

        except smtplib.SMTPException:
            print("Error: 无法发送邮件")


    def send_main(self,file_list, pass_list,fail_list,error_list,no_run_list):
        pass_num = len(pass_list)
        fail_num = len(fail_list)
        error_num = len(error_list)
        #未执行的用例
        no_run_num = len(no_run_list)
        count_num = pass_num + fail_num + no_run_num + error_num

        #成功率、失败率
        pass_result = "%.2f%%" % (pass_num/count_num*100)
        fail_result = "%.2f%%" % (fail_num/count_num*100)
        error_result = "%.2f%%" % (error_num / count_num * 100)
        no_run_result = "%.2f%%" % (no_run_num/count_num*100)

        # receiver_list = ['xxx@dianhun.cn','xxx@qq.com']
        receiver_list = ['laiminda@dianhun.cn']  # 接收邮件，可设置为你的邮箱
        sub = "接口自动化测试报告"
        content = "接口自动化测试结果:\n通过个数%s个，失败个数%s个，错误个数%s个，未执行个数%s个：通过率为%s，失败率为%s，错误率为%s，未执行率为%s\n日志见附件" % \
                  (pass_num, fail_num, error_num, no_run_num, pass_result, fail_result, error_result, no_run_result)
        self.send_mail(file_list, receiver_list, sub, content)