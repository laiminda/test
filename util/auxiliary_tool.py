#!/usr/bin/python
# -*- coding: UTF-8 -*-
from datetime import datetime
import os


def print_time(date):
    time_list = date.split(':')
    tim = ''
    if int(time_list[0]):
        tim += (time_list[0] + '小时')
    if int(time_list[1]):
        tim += (time_list[1] + '分钟')
    if int(time_list[2].split('.')[0]):
        tim += (time_list[2].split('.')[0] + '秒')
    if int(time_list[2].split('.')[1]):
        tim += (time_list[2].split('.')[1] + '微秒')
    return tim


def mkdir(path='../log'):
    time = datetime.now()
    tm = time.strftime("%Y-%m-%d %H:%M:%S")
    dirname=str(tm).split()[0]
    pa = os.path.join(path, dirname)
    folder = os.path.exists(pa)
    if not folder:
        os.makedirs(pa)
        return True
    else:
        return False

def mkfile(path='../log', extension='.txt'):
    time = datetime.now()
    tm = time.strftime("%Y-%m-%d %H:%M:%S")
    dirname = str(tm).split()[0]
    filename = str(tm).split()[1].replace(':','_')
    file_path = path+'/'+dirname+'/'+filename+extension
    with open(file_path, 'wb') as f:
        f.seek(0, 0)  # 加上f.seek(0)，把文件定位到position 0;没有这句的话，文件是定位到数据最后，truncate也是从最后这里删除
        f.truncate()
    return file_path