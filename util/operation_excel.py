#!/usr/bin/python
# -*- coding: UTF-8 -*-
import openpyxl as openpyxl
import xlrd
from openpyxl.styles import Font
from xlutils.copy import copy

class OperationExcel:
    def __init__(self,file_name='../case/interface.xls',sheet_id=0):
        self.file_name = file_name
        self.sheet_id = sheet_id
        self.currentpage = sheet_id
        self.file = self.get_data()

    # 获取当前文档所有sheet数
    def get_sheet_num(self):
        return self.file.sheet_names().__len__()

    # 切换到下一个sheet
    def turn_page(self):
        if self.currentpage == self.get_sheet_num():
            return None
        sheetname = self.file.sheet_names()[self.currentpage]
        self.data = self.file.sheets()[self.currentpage]
        self.currentpage += 1
        return sheetname

    #获取整个文件的内容
    def get_data(self):
        file = xlrd.open_workbook(self.file_name)
        return file

    #获取单元格的行数
    def get_lines(self):
        tables = self.data
        return tables.nrows

    #获取某一个单元格的内容
    def get_cell_value(self,row,col):
        tables = self.data
        cell = tables.cell_value(row,col)
        return cell

    #写入数据
    def write_value(self,row,col,value):
        '''
        写入到excel数据
        row,col,value
        '''
        read_data = xlrd.open_workbook(self.file_name)
        write_data = copy(read_data)
        sheet_data = write_data.get_sheet(0)
        sheet_data.write(row,col,value)
        write_data.save(self.file_name)

    #根据对应case_id找到对应行的内容
    def get_rows_data(self,case_id):
        row_num = self.get_row_num(case_id)
        return  self.get_row_values(row_num)

    #根据对应的case_id找到对应的行号
    def get_row_num(self,case_id):
        num = 0
        coldata = self.get_col_values()
        for data in coldata:
            if case_id in data:
                return num
            num+=1
        return num

    #根据行号，找到该行的数据
    def get_row_values(self,row):
        tables = self.data
        row_data = tables.row_values(row)
        return row_data

    # 根据列号，找到该列的数据
    def get_col_values(self,col=0):
        col_data = self.data.col_values(col)
        return col_data


class OperationXlsx:
    def __init__(self,file_name='../case/interface.xlsx',sheet_id=0):
        self.file_name = file_name
        self.sheet_id = sheet_id
        self.loadfile = self.get_data()
        self.currentpage=sheet_id

    # 获取当前文档所有sheet数
    def get_sheet_num(self):
        return self.loadfile.sheetnames.__len__()

    # 切换到下一个sheet
    def turn_page(self):
        if self.currentpage == self.get_sheet_num():
            return None
        sheetname = self.loadfile.sheetnames[self.currentpage]
        self.data=self.loadfile[sheetname]
        self.currentpage += 1
        return sheetname

    def set_font(self, cell):
        workbook = openpyxl.load_workbook(filename=self.file_name)
        font = Font(name="Arial")
        cell.font = font
        workbook.save(filename=self.file_name)

    #获取整个文件的内容
    def get_data(self):
        loadfile = openpyxl.load_workbook(self.file_name)
        return loadfile

    #获取单元格的行数
    def get_lines(self):
        tables = self.data.max_row
        return tables

    #获取某一个单元格的内容
    def get_cell_value(self,row,col):
        tables = self.data
        #self.set_font(tables.cell(row,col))
        cell = tables.cell(row,col).value
        return cell

    #写入数据
    def write_value(self,row,col,value):
        '''
        写入到excel数据
        row,col,value
        '''
        tables = self.data
        tables.cell(row, col).value = value
        self.loadfile.save(self.file_name)

    #根据对应case_id找到对应行的内容
    def get_rows_data(self,case_id):
        row_num = self.get_row_num(case_id)
        return self.get_row_values(row_num)

    #根据对应的case_id找到对应的行号
    def get_row_num(self,case_id):
        num = 1
        coldata = self.get_col_values()
        for data in coldata:
            if case_id in data:
                return num
            num+=1
        return num

    #根据行号，找到该行的数据
    def get_row_values(self,row):
        columns = self.data.max_column
        rowdata = []
        for i in range(1, columns + 1):
            cellvalue = self.data.cell(row=row, column=i).value
            rowdata.append(cellvalue)
        return rowdata

    # 根据列号，找到该列的数据
    def get_col_values(self,col=1):
        rows = self.data.max_row
        columndata = []
        for i in range(1, rows + 1):
            cellvalue = self.data.cell(row=i, column=col).value
            columndata.append(cellvalue)
        return columndata
