#!/usr/bin/python
# -*- coding: UTF-8 -*-
import xml.sax


class xmlparser(xml.sax.handler.ContentHandler ):
    def __init__(self):
        super().__init__()
        self.CurrentData=""
        self.casepath=""
        self.paths=[]

    def startElement(self, tag, attributes):
        self.CurrentData = tag

    def endElement(self, tag):
        if self.CurrentData == "testcase":
            self.paths.append(self.casepath)
        self.CurrentData = ""

    def characters(self,content):
        if self.CurrentData == "testcase" and "\\" in content:
            self.casepath=content
