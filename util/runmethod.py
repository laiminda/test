#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json

import requests
TIMEOUT = 3

class RunMethod:
    def __init__(self):
        self.elapsed = -1
    def post_main(self, url, data=None, header=None):
        # if '&uid=&token=' in url:
        data = json.dumps(data)
        if header != None:
            res = requests.post(url=url, data=data, headers=header, timeout=TIMEOUT)
        else:
            res = requests.post(url=url, data=data, timeout=TIMEOUT)
        return res

    def get_main(self, url, params=None, header=None, cookie=None):
        if header != None:
            res = requests.get(url=url, params=params, headers=header, cookies=cookie, timeout=TIMEOUT)
        else:
            res = requests.get(url=url, params=params, cookies=cookie, timeout=TIMEOUT)
        return res

    def head_main(self, url, params=None, header=None):
        if header != None:
            res = requests.head(url=url, params=params, headers=header, timeout=TIMEOUT)
        else:
            res = requests.head(url=url, params=params, timeout=TIMEOUT)
        return res

    def patch_main(self, url, data=None, header=None):
        if header != None:
            res = requests.patch(url=url, data=data, headers=header, timeout=TIMEOUT)
        else:
            res = requests.patch(url=url, data=data, timeout=TIMEOUT)
        return res

    def put_main(self, url, data=None, header=None):
        if header != None:
            res = requests.put(url=url, data=data, headers=header, timeout=TIMEOUT)
        else:
            res = requests.put(url=url, data=data, timeout=TIMEOUT)
        return res

    def delete_main(self, url, header=None):
        if header != None:
            res = requests.delete(url=url, headers=header, timeout=TIMEOUT)
        else:
            res = requests.delete(url=url, timeout=TIMEOUT)
        return res

    def run_main(self, method, url, data=None, header=None, params=None, cookie=None):
        med = method.upper()
        if med == 'POST':
            res = self.post_main(url, data, header)
        elif med == 'GET':
            res = self.get_main(url, params, header, cookie)
        elif med == 'HEAD':
            res = self.head_main(url, params, header)
        elif med == 'PATCH':
            res = self.patch_main(url, data, header)
        elif med == 'PUT':
            res = self.put_main(url, data, header)
        elif med == 'DELETE':
            res = self.delete_main(url, header)
        else:
            res = requests.request(method=med, url=url, data=data, header=header, params=params, timeout=TIMEOUT)
        #print(res.elapsed)
        self.elapsed = res.elapsed
        return res.json()
